import os
import re
import sys

#models = {"a14", "a725", "x925", "m4", "m4-0"}
model = sys.argv[2]
benches = {"perlbench", "mcf", "lbm",
           "xalancbmk", "x264", "deepsjeng",
           "leela", "xz", "gcc",
           "omnetpp", "nab"} 


def extract_values_from_stats(file_path, value):
    """
    Extract values for the specified lines from a stats.txt file.

    Args:
        file_path (str): Path to the stats.txt file.

    Returns:
        tuple: A tuple containing the two extracted integer values, or None if not found.
    """
    mem_order_violation_events = None
    num_insts = None
    cpi = None

    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith('system.switch_cpus.commit.memOrderViolationEvents'):
                mem_order_violation_events = float(line.split()[1])
            elif line.startswith('system.switch_cpus.commitStats0.numInsts'):
                num_insts = float(line.split()[1])
            elif line.startswith('CPI'):
                cpi = float(line.split()[1])

            # Break early if both values are found
            if mem_order_violation_events is not None and num_insts is not None and cpi is not None:
                break

    if value == "mpki":
        return 1024*mem_order_violation_events/num_insts
    else:
        return cpi

def build_directory_stats(root, value):
    """
    Build a dictionary mapping directory names to extracted values.

    Args:
        base_path (str): The base directory to start searching from.

    Returns:
        dict: A dictionary mapping directory names to tuples of extracted values.
    """
    results = {}

    for root, dirs, files in os.walk(root):
        for dir_name in dirs:
            if dir_name.split(".")[0] in benches:
                bench = dir_name
                if bench not in results:
                    results[bench] = {}
            stats_path = os.path.join(root, dir_name, 'results.txt')
            if os.path.isfile(stats_path) and stats_path.split("/")[6] == model and stats_path.split("/")[4] in ['storesets','main-phast']:
                result = extract_values_from_stats(stats_path, value)
                predictor = stats_path.split("/")[4]
                results[bench][predictor] = result

    return results

if __name__ == "__main__":
    value = sys.argv[1];
    if value != "mpki" and value != "cpi": print("invalid value"); exit(1)
    base_directory = os.getcwd()  
    stats_dict = build_directory_stats(base_directory, value)

    if value == "mpki":    
        for bench in stats_dict:
            print(bench+":")
            for predictor, result in stats_dict[bench].items():
                print(f"{predictor}: MPKI={result}")
            print()
    else:
        for bench in stats_dict:
            print(bench+":")
            for predictor, result in stats_dict[bench].items():
                print(f"{predictor}: CPI={result}")
            print()
