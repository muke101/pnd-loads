import matplotlib.pyplot as plt
import re
from collections import defaultdict
import sys
import subprocess

value = sys.argv[1]
value_caps = ''.join([c.upper() for c in value])
model = sys.argv[2]

# Input data
data = subprocess.run("pypy3 /work/muke/PND-Loads/utils/collect_results.py "+value+" "+model, shell=True, check=True, capture_output=True, text=True).stdout

# Parse the input data
def parse_data(data):
    benchmarks = defaultdict(dict)
    current_benchmark = None

    for line in data.strip().split("\n"):
        line = line.strip()
        if line.endswith(":"):
            current_benchmark = line[:-1]
        elif value_caps in line:
            category, mpki = re.match(r"(.+?): "+value_caps+"=(.+)", line).groups()
            benchmarks[current_benchmark][category] = float(mpki)
    
    return benchmarks

# Prepare data for plotting
benchmarks = parse_data(data)
categories = ["storesets", "main-phast"]
x_labels = ["perlbench.0", "perlbench.1", "perlbench.2", "gcc.0", "gcc.1", "gcc.2", "mcf.0", "lbm.0", "omnetpp.0", "xalancbmk.0", "x264.0", "x264.1", "x264.2", "deepsjeng.0", "leela.0", "nab.0", "xz.0", "xz.1"]
y_values = {cat: [benchmarks[bm].get(cat, 0) for bm in x_labels] for cat in categories}

# Plot the data
fig, ax = plt.subplots(figsize=(12, 6))

x_indices = range(len(x_labels))
width = 0.2  # Bar width

previous_cat = None
for i, category in enumerate(categories):
    bars = ax.bar(
        [x + i * width for x in x_indices],
        y_values[category],
        width,
        label=category,
    )

cat1, cat2 = categories
if value == "cpi": rot = 0
elif value == "mpki": rot = 90
for i in range(len(x_labels)):
    val1 = y_values[cat1][i]
    val2 = y_values[cat2][i]
    
    if val1 > 0:  # Avoid division by zero
        percent_diff = ((val2 - val1) / val1) * 100
        x_pos = x_indices[i] + width * 1.1 # Position between the two bars
        ax.text(x_pos, val2 + 0.05, f"{percent_diff:.1f}%", 
                ha='center', va='bottom', fontsize=9, rotation=rot)


for i in range(len(x_labels)):
    plt.axvline(x=i-(0.75*width), color='grey', linestyle=':', linewidth=1)

# Customize the plot
ax.set_xlabel("Benchmarks", fontsize=12)
ax.set_ylabel(value_caps, fontsize=12)
ax.set_title(value_caps+" on "+model, fontsize=14)
ax.set_xticks([x + width for x in x_indices])
ax.set_xticklabels(x_labels, rotation=45, ha="right")
ax.legend(title="Categories")
ax.grid(axis="y", linestyle="--", alpha=0.7)

plt.tight_layout()
plt.savefig("./spec-"+value+"-"+model+".png", dpi=200)
