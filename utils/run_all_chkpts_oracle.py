import subprocess
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import psutil
import time

oracle_mode = sys.argv[1]
run_type = sys.argv[2]
cpu_model = sys.argv[3]
chkpt_dir = "/mnt/data/checkpoints/"
results_dir = "/mnt/data/results/pnd-labels/"+run_type+"/base/"+cpu_model+"/"
benches = ["600.perlbench_s", "605.mcf_s", "619.lbm_s",
           "623.xalancbmk_s", "625.x264_s", "631.deepsjeng_s",
           "641.leela_s", "657.xz_s", "602.gcc_s",
           "620.omnetpp_s", "644.nab_s", "648.exchange2_s"] #"638.imagick_s"]

if len(sys.argv) > 4:
    sub_benches = []
    for bench in sys.argv[4:]:
        if bench in benches:
            sub_benches.append(bench)
        else:
            print("Unknown benchmark: ", bench)
            exit(1)
    benches = sub_benches


#run checkpoints
processes = []
for bench in benches:
    if bench == "648.exchange2_s":
        os.chdir("/mnt/data/checkpoints-expanded/648.exchange2_s")
    else:
        os.chdir(chkpt_dir+bench)

    while psutil.virtual_memory().percent > 70 and psutil.cpu_ercent() > 95: time.sleep(60*5)
    p = subprocess.Popen("python3 /work/muke/PND-Loads/utils/spec_automation_oracle.py "+oracle_mode+" "+run_type+" "+cpu_model, shell=True)
    processes.append(p)

for p in processes:
    code = p.wait()
    if code is not None and code != 0: print(p.args); exit(1)

#aggregate stats
for bench in benches:
    name = bench.split(".")[1].split("_")[0]
    
    for i in (0,1,2):
        if os.path.exists(results_dir+name+'.'+str(i)):
            raw_results_dir = results_dir+name+'.'+str(i)+"/raw/"
            os.chdir(raw_results_dir)
            p = subprocess.Popen("python3 /work/muke/PND-Loads/utils/aggregate_stats.py "+bench+" "+str(i), shell=True)
            p.wait()
            subprocess.Popen("cp results.txt ../", shell=True)
            raw_results_dir = results_dir+name+'.'+str(i)+"/raw/"
            os.chdir(raw_results_dir)
            p = subprocess.Popen("python3 /work/muke/PND-Loads/utils/aggregate_stats.py "+bench+" "+str(i), shell=True)
            p.wait()
            subprocess.Popen("cp results.txt ../", shell=True)

if oracle_mode == "refine":
    subprocess.run("rm -rf /mnt/data/results/pnd-labels/last_refinement", shell=True, check=True)
    subprocess.run("cp -r "+results_dir+" /mnt/data/results/pnd-labels/last_refinement", shell=True, check=True)
