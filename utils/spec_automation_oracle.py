import os
import sys
import subprocess
from subprocess import Popen
import psutil
import random
import time

cache_sizes = {
    "default": "",
    "scaled": " --l1d_size=128KiB --l1i_size=128KiB --l2_size=2MB",
    "very_scaled": " --l1d_size=256KiB --l1i_size=256KiB --l2_size=4MB",
    "x925": " --l1d_size=64KiB --l1i_size=64KiB --l2_size=4MB",
    "a725": " --l1d_size=64KiB --l1i_size=64KiB --l2_size=1MB",
    "a14": " --l1d_size=64KiB --l1i_size=128KiB --l2_size=4MB",
    "a14-tournament": " --l1d_size=64KiB --l1i_size=128KiB --l2_size=4MB",
    "a14-small-mdp": " --l1d_size=64KiB --l1i_size=128KiB --l2_size=4MB",
    "m4": " --l1d_size=128KiB --l1i_size=256KiB --l2_size=16MB",
    "m4-0": " --l1d_size=128KiB --l1i_size=256KiB --l2_size=16MB",
}

#run from base spec dir
base_dir = os.getcwd() # = /work/muke/checkpoints/benchmark
oracle_mode = sys.argv[1]
run_type = sys.argv[2]
cpu_model = sys.argv[3]
spec_path = "/work/muke/spec2017/"
gem5 = "/work/muke/MDP-Oracle/gem5/"
results_dir = "/mnt/data/results/pnd-labels/"+run_type+"/base/"+cpu_model+"/"
benchmark = base_dir.split("/")[4]
run_dir = spec_path+"benchspec/CPU/"+benchmark+"/run/run_peak_refspeed_mytest-64.0000/"
os.chdir(run_dir)
specinvoke = subprocess.run([spec_path+"bin/specinvoke", "-n"], stdout=subprocess.PIPE)
commands = [line.decode().strip() for line in specinvoke.stdout.split(b"\n") if not line.startswith(b"#")]
os.chdir(base_dir)
random.seed(sum(ord(c) for c in base_dir))
procs = []

#iterate over all checkpoint.n dirs
for out_dir in os.listdir(base_dir):
    if benchmark == "648.exchange2_s" and out_dir != "checkpoints.0": continue
    run_number = out_dir[-1]
    command = commands[int(run_number)]
    command = command.split('>')[0]
    cpt_number = 0
    out_dir = os.path.join(base_dir,out_dir)
    #iterate over checkpoint.n
    for chkpt_dir in os.listdir(out_dir):
        #find cpt.m dir
        if os.path.isdir(os.path.join(out_dir, chkpt_dir)) and chkpt_dir.startswith('cpt.'):
            waited = 0
            finished = False
            cpt_number += 1
            binary = run_dir+command.split()[0]
            benchmark_name = benchmark.split("_")[0].split(".")[1]
            outdir = results_dir+benchmark_name+"."+run_number+"/raw/"
            if not os.path.exists(outdir): os.makedirs(outdir) #create the parent directories for gem5 stats dir if needed
            outdir += str(cpt_number)+".out"
            trace_dir = results_dir+benchmark+"/traces/"+str(cpt_number)
            if not os.path.exists(trace_dir):
                os.makedirs(trace_dir)
            oracle_mode = oracle_mode[0].upper()+oracle_mode[1:]
            run = "ORACLEMODE="+oracle_mode+" TRACEDIR="+trace_dir+" "+gem5+"build/ARM/gem5.fast --outdir="+outdir+" "+gem5+"configs/deprecated/example/se.py --cpu-type=DerivO3CPU --caches --l2cache --restore-simpoint-checkpoint -r "+str(cpt_number)+" --checkpoint-dir "+out_dir+" --restore-with-cpu=AtomicSimpleCPU --mem-size=50GB -c "+binary+" --options=\""+' '.join(command.split()[1:])+"\""
            run += cache_sizes[cpu_model]
            os.chdir(run_dir)
            while psutil.virtual_memory().percent > 60 and psutil.cpu_percent() > 90: time.sleep(60*5)
            p = Popen(run, shell=True)
            os.chdir(base_dir)
            procs.append(p)
            while waited < 60*5 and finished == False:
                time.sleep(10)
                waited += 10
                if Popen.poll(p) != None:
                    finished = True
                    if Popen.wait(p) != 0: print(p.args); exit(1)
            time.sleep(random.uniform(0,1)*60)
            if psutil.virtual_memory().percent < 60 and psutil.cpu_percent() < 90: continue
            if Popen.wait(p) != 0: print(p.args); exit(1)

for p in procs:
    code = Popen.wait(p)
    if code is not None and code != 0: print(p.args); exit(1)
